#ifndef _SIRCD_H_
#define _SIRCD_H_

#include <sys/types.h>
#include <netinet/in.h>

#define MAX_CLIENTS 512
#define MAX_MSG_TOKENS 10
#define MAX_MSG_LEN 512
#define MAX_USERNAME 32
#define MAX_HOSTNAME 512
#define MAX_SERVERNAME 512
#define MAX_REALNAME 512
#define MAX_CHANNAME 512

typedef struct {
    	int sock;
    	struct sockaddr_in cliaddr;
    	unsigned inbuf_size;               // Size of the buffer that stores message
    	int registered;                    // Checks if client is registered. -1 for no, 1 for yes.
    	char hostname[MAX_HOSTNAME];       // Host name of client
    	char servername[MAX_SERVERNAME];   // Name of the server which the client is connected to
    	char user[MAX_USERNAME];           // Client's username
        char nick[MAX_USERNAME];           // Client's nickname
    	char realname[MAX_REALNAME];       // Client's realname
    	char inbuf[MAX_MSG_LEN+1];         // Buffer that stores client message
        char channel[MAX_CHANNAME];        // Name of channel user is currently on
        int channelID;                     // ID of Channel user is on (-1 if no channel)
        int user_registered;               // Has the client set their user values? -1 for no, 1 for yes.
        int nick_registered;               // Has the client set their nickname? -1 for no, 1 for yes.
} client;

typedef struct {
	int ID;	                   // Channel ID
	char name[MAX_CHANNAME];   // Channel name
	int num_of_clients;	       // Number of connected clients on channel
} channel;

#endif /* _SIRCD_H_ */
