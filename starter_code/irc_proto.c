#include "irc_proto.h"
#include "debug.h"

#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <fcntl.h>
#include "sircd.h"

#define MAX_COMMAND 16

/* You'll want to define the CMD_ARGS to match up with how you
 * keep track of clients.  Probably add a few args...
 * The command handler functions will look like
 * void cmd_nick(CMD_ARGS)
 * e.g., void cmd_nick(your_client_thingy *c, char *prefix, ...)
 * or however you set it up.
 */

#define CMD_ARGS char *prefix, char **params, int n_params, int clientID, client *clients, channel *channels, fd_set *master
typedef void (*cmd_handler_t)(CMD_ARGS);
#define COMMAND(cmd_name) void cmd_name(CMD_ARGS)
    

struct dispatch {
    char cmd[MAX_COMMAND];
    int needreg; /* Must the user be registered to issue this cmd? */
    int minparams; /* send NEEDMOREPARAMS if < this many params */
    cmd_handler_t handler;
};


#define NELMS(array) (sizeof(array) / sizeof(array[0]))

/* Define the command handlers here.  This is just a quick macro
 * to make it easy to set things up */
COMMAND(cmd_nick);
COMMAND(cmd_user);
COMMAND(cmd_quit);
COMMAND(cmd_join);
COMMAND(cmd_part);
COMMAND(cmd_list);
COMMAND(cmd_privmsg);
COMMAND(cmd_who);

/* Dispatch table.  "reg" means "user must be registered in order
 * to call this function".  "#param" is the # of parameters that
 * the command requires.  It may take more optional parameters.
 */
struct dispatch cmds[] = {
    /* cmd,    reg  #parm  function */
	{ "NICK", 0, 1, cmd_nick },
	{ "USER", 0, 4, cmd_user },
	{ "QUIT", 0, 0, cmd_quit },
	{ "JOIN", 1, 1, cmd_join },
	{ "PART", 1, 1, cmd_part },
	{ "LIST", 0, 0, cmd_list },
	{ "PRIVMSG", 1, 1, cmd_privmsg },
	{ "WHO", 0, 1, cmd_who },
};

/*
	Helper function declarations
*/

void send_error(char *error, err_t error_no, int clientID, client *clients);
void send_message(char *message, int clientID, client *clients);
void send_privmsg(char *message, int senderID, int receiverID, client *clients);
void broadcast(char *message, client *clients, int channelID, int num_of_clients);
void resgister_client(int clientID, client *clients);

// Send a formatted error message to client
void send_error(char *error, err_t error_no, int clientID, client *clients) {
    char error_message[MAX_MSG_LEN];
    memset(error_message, '\0', sizeof(error_message));
    sprintf(error_message, "Error %d: %s", error_no, error);
    send_message(error_message, clientID, clients);
}

// Send message to client
void send_message(char *message, int clientID, client *clients) {
    char msg[MAX_MSG_LEN];
    memset(msg, '\0', sizeof(msg));
    sprintf(msg, "%s\r\n", message);
    writen(clients[clientID].sock, msg, sizeof(msg));
}

// Send private message from one client (sender) to another (receiver)
void send_privmsg(char *message, int senderID, int receiverID, client *clients) {
    char pm[MAX_MSG_LEN];
    memset(pm, '\0', sizeof(pm));

    // Format the message and then send it to the receiver
    sprintf(pm, ":%s PRIVMSG %s :%s", clients[senderID].nick, clients[receiverID].nick, message);
    send_message(pm, receiverID, clients);
}

// Echo message to all clients connected to a channel
void broadcast(char *message, client *clients, int channelID, int num_of_clients) {
    int i;
    for (i = 0; i < MAX_CLIENTS; i++) {
        // If client is registered and on the channel, send the message
        if (clients[i].registered > 0 && channelID == clients[i].channelID) {
            send_message(message, i, clients);
            num_of_clients--;
        }
        // For efficiency
        if (num_of_clients <= 0) break;
    }
}

void resgister_client(int clientID, client *clients) {
    if (clients[clientID].user_registered == 1 && clients[clientID].nick_registered == 1) {
        clients[clientID].registered = 1;
    }

    return;
}

/* Handle a command line.  NOTE:  You will probably want to
 * modify the way this function is called to pass in a client
 * pointer or a table pointer or something of that nature
 * so you know who to dispatch on...
 * Mostly, this is here to do the parsing and dispatching
 * for you
 *
 * This function takes a single line of text.  You MUST have
 * ensured that it's a complete line (i.e., don't just pass
 * it the result of calling read()).  
 * Strip the trailing newline off before calling this function.
 */

void
handle_line(char *line, int clientID, client * clients, channel * channels, fd_set * socketset)
{
    char *prefix = NULL, *command = NULL, *pstart, *params[MAX_MSG_TOKENS];
    int n_params = 0;
    char *trailing = NULL;

    DPRINTF(DEBUG_INPUT, "Handling line: %s\n", line);

    command = line;
    
    if (*line == ':') {
    prefix = ++line;
    command = strchr(prefix, ' ');
    }
    if (!command || *command == '\0') {
        /* Send an unknown command error! */
        send_error("ERR_UNKNOWNCOMMAND 1 ", ERR_UNKNOWNCOMMAND, clientID, clients);
        return;
    }
    
    while (*command == ' ') {
        *command++ = 0;
    }
    if (*command == '\0') {
        /* Send an unknown command error! */
        send_error("ERR_UNKNOWNCOMMAND 2 ", ERR_UNKNOWNCOMMAND, clientID, clients);
        return;
    }
    pstart = strchr(command, ' ');
    if (pstart) {
    while (*pstart == ' ') {
        *pstart++ = '\0';
    }
    if (*pstart == ':') {
        trailing = pstart;
    } else {
        trailing = strstr(pstart, " :");
    }
    if (trailing) {
        while (*trailing == ' ')
        *trailing++ = 0;
        if (*trailing == ':')
        *trailing++ = 0;
    }
    
    do {
        if (*pstart != '\0') {
        params[n_params++] = pstart;
        } else {
        break;
        }
        pstart = strchr(pstart, ' ');
        if (pstart) {
        while (*pstart == ' ') {
            *pstart++ = '\0';
        }
        }
    } while (pstart != NULL && n_params < MAX_MSG_TOKENS);
    }

    if (trailing && n_params < MAX_MSG_TOKENS) {
    params[n_params++] = trailing;
    }
    

    DPRINTF(DEBUG_INPUT, "Prefix:  %s\nCommand: %s\nParams (%d):\n",
       prefix ? prefix : "<none>", command, n_params);
    int i;
    for (i = 0; i < n_params; i++) {
        DPRINTF(DEBUG_INPUT, "   %s\n", params[i]);
    }
    DPRINTF(DEBUG_INPUT, "\n");

    for (i = 0; i < NELMS(cmds); i++) {
        if (!strcasecmp(cmds[i].cmd, command)) {
            if (cmds[i].needreg && clients[clientID].registered != 1 ) { /* checking if client is registered */
                    /* ERROR THAT CLIENT IS NOT REGISTERED */

                    send_error("ERR_NOTREGISTERED ", ERR_NOTREGISTERED, clientID, clients);
                    return;

                 //ERROR - the client is not registered and they need
                 //* to be in order to use this command! 

            } else if (n_params < cmds[i].minparams) {
                /* ERROR - the client didn't specify enough parameters
                 * for this command! */
                    send_error("ERR_NEEDMOREPARAMS ", ERR_NEEDMOREPARAMS, clientID, clients);
                    return;
            } else {
                /* Here's the call to the cmd_foo handler... modify
                 * to send it the right params per your program
                 * structure. */
                (*cmds[i].handler)(prefix, params, n_params, clientID, clients, channels, socketset);
            }
            break;
        }
     }
    if (i == NELMS(cmds)) {
        /* ERROR - unknown command! */
        send_error("ERR_UNKNOWNCOMMAND ", ERR_UNKNOWNCOMMAND, clientID, clients);
        return;
    }
}



/* Command handlers */
/* MODIFY to take the arguments you specified above! */
void cmd_nick(char *prefix, char **params, int n_params, int clientID, client *clients, channel *channels, fd_set *master)
{
    // The nickename is stored at first index of params, i.e., params[0]
    int i;
    char message[MAX_MSG_LEN];
    memset(message, '\0', sizeof(message));

    // Check if the nickname is taken
    for(i = 0; i < MAX_CLIENTS; i++) {
        if (i != clientID) {
            if(strcasecmp(clients[i].nick, params[0]) == 0){
                send_error("ERR_NICKNAMEINUSE", ERR_NICKNAMEINUSE, clientID, clients);
                break;
            }
        }
    }
    // Else if nick is not found, then create the nick
    if(i == MAX_CLIENTS){
        // Check if client does not have a nick already.
        if (clients[clientID].nick_registered == -1) {
            strcpy(clients[clientID].nick, params[0]);
            sprintf(message, ":%s NICK_REGISTERED", clients[clientID].nick);
            clients[clientID].nick_registered = 1;
            resgister_client(clientID, clients);
            send_message(message, clientID, clients);
        }
        else {
            char old_nick[MAX_USERNAME];
            memset(old_nick, '\0', sizeof(old_nick));
            strcpy(old_nick, clients[clientID].nick); // Store old nick name

            strcpy(clients[clientID].nick, params[0]);
            sprintf(message, ":%s changed NICK to %s", old_nick, clients[clientID].nick);
            
            if (clients[clientID].channelID > 0) {
                broadcast(message, clients, clients[clientID].channelID, channels[clients[clientID].channelID].num_of_clients);
            } else {
                send_message(message, clientID, clients);
            }
        }

    }

    return;
}

// User requires 4 parameters: <username> <hostname> <servername> <realname>
// These are stored in params[0], params[1], params[2] and params[3], respectively.
void cmd_user(char *prefix, char **params, int n_params, int clientID, client *clients, channel *channels, fd_set *master)
{
    client cli = clients[clientID];
    if (clients[clientID].user_registered > 0) {
        send_error("ERR_ALREADYREGISTRED", ERR_ALREADYREGISTRED, clientID, clients);
    } else {
        sprintf(clients[clientID].user, "%s", params[0]);
        sprintf(clients[clientID].hostname, "%s", params[1]);
        sprintf(clients[clientID].servername, "%s", params[2]);
        sprintf(clients[clientID].realname, "%s", params[3]);

        clients[clientID].user_registered = 1;
        resgister_client(clientID, clients);
        send_message("USER_REGISTERED", clientID, clients);
    }


    return;
}

void cmd_quit(char *prefix, char **params, int n_params, int clientID, client *clients, channel *channels, fd_set *master)
{
    char message[MAX_MSG_LEN];
    memset(message, '\0', sizeof(message));

    // If user sends no quit message, send default message. Otherwise, send user message.
    if (n_params == 0) {
        sprintf(message, ":%s QUIT :Connection closed", clients[clientID].nick);
    }
    else if (n_params == 1) {
        sprintf(message, ":%s QUIT :%s", clients[clientID].nick, params[0]);
    }

    // If client is registered, notify all clients on channel that user has quit
    if (clients[clientID].registered == 1) {
        broadcast(message, clients, clients[clientID].channelID, channels[clients[clientID].channelID].num_of_clients);
    }

    // Close connection and clear the client information
    close(clients[clientID].sock);
    FD_CLR(clients[clientID].sock, master);
    clients[clientID].sock = -1;
    clients[clientID].user_registered = -1;
    clients[clientID].nick_registered = -1;
    clients[clientID].registered = -1;
    channels[clients[clientID].channelID].num_of_clients--;
    clients[clientID].channelID = -1;

}

// The first parameter, i.e. params[0], is the name of the channel
void cmd_join(char *prefix, char **params, int n_params, int clientID, client *clients, channel *channels, fd_set *master)
{
    int channelID = -1;
    char join_message[MAX_MSG_LEN], part_message[MAX_MSG_LEN];

    memset(join_message, '\0', sizeof(join_message));
    memset(part_message, '\0', sizeof(part_message));

    if (strlen(params[0]) > MAX_CHANNAME) {
        send_error("ERR_CHANNEL_NAME_TOO_LONG", ERR_INVALID, clientID, clients);
        return;
    }

    if (clients[clientID].channelID > -1) {
        channelID = clients[clientID].channelID;
    }

    // Check if client is registered.
    if (clients[clientID].registered == 1) {

        // Loop through the channel list to check if there exists a channel with the same name
        int i;
        for (i = 0; i < MAX_CLIENTS; i++) {
            if ( (channels[i].num_of_clients > 0) && (strcmp(channels[i].name, params[0]) == 0) ) {
                // If client enters same channel name as the one they're on
                if (strcmp(channels[clients[clientID].channelID].name, params[0]) == 0) {
                    send_message("You are already connected to this channel!", clientID, clients);
                    return;
                }
                // If client already has a channel, PART from it
                if (channelID > -1) {
                    sprintf(part_message, ":%s PARTED %s", clients[clientID].nick, channels[channelID].name);
                    broadcast(part_message, clients, clients[clientID].channelID, channels[channelID].num_of_clients);
                    channels[channelID].num_of_clients--;
                    clients[clientID].channelID = -1;
                }

                channels[i].num_of_clients++;
                clients[clientID].channelID = channels[i].ID;
                sprintf(join_message, ":%s JOINED %s", clients[clientID].nick, channels[i].name);
                broadcast(join_message, clients, clients[clientID].channelID, channels[i].num_of_clients);
                break;
            }
        }

        // If channel does not exist in the channel list, create it
        if (i == MAX_CLIENTS) {
            int j;
            for (j = 0; j < MAX_CLIENTS; j++) {
                if (channels[j].num_of_clients == 0 && (strcmp(channels[j].name, params[0]) != 0) ) {
                    // If client already has a channel, PART from it
                    if (channelID > -1) {
                        sprintf(part_message, ":%s PARTED %s", clients[clientID].nick, channels[channelID].name);
                        broadcast(part_message, clients, clients[clientID].channelID, channels[channelID].num_of_clients);
                        channels[channelID].num_of_clients--;
                        clients[clientID].channelID = -1;
                    }
                    channels[j].num_of_clients++;
                    clients[clientID].channelID = channels[j].ID;
                    sprintf(channels[j].name, "%s", params[0]);
                    sprintf(join_message, "%s JOINED %s", clients[clientID].nick, channels[clients[clientID].channelID].name);
                    broadcast(join_message, clients, clients[clientID].channelID, channels[clients[clientID].channelID].num_of_clients);
                    break;
                }
            }
            if (j == MAX_CLIENTS) {
                send_error("ERR_CANT_CREATE_CHANNEL", ERR_INVALID, clientID, clients);
                return;
            }
        }
        
    } else {
        send_error("ERR_NOTREGISTERED", ERR_NOTREGISTERED, clientID, clients);
    }

    return;
}
// The first parameter, i.e. params[0], is the name of the channel the client wants to part with
void cmd_part(char *prefix, char **params, int n_params, int clientID, client *clients, channel *channels, fd_set *master)
{
    char message[MAX_MSG_LEN];
    memset(message, '\0', sizeof(message));

    // Check if channel exists
    int i;
    for (i = 0; i < MAX_CLIENTS; i++) {
        if (strcmp(channels[i].name, params[0]) == 0) {
            break;
        }
    }
    if (i == MAX_CLIENTS) {
        send_error("ERR_NOSUCHCHANNEL", ERR_NOSUCHCHANNEL, clientID, clients);
        return;
    }

    // Check if clients enters the name of the channel they're connected to
    if (strcmp(channels[clients[clientID].channelID].name, params[0]) == 0) {
        sprintf(message, "%s PARTED %s", clients[clientID].nick, channels[clients[clientID].channelID].name);
        broadcast(message, clients, clients[clientID].channelID, channels[clients[clientID].channelID].num_of_clients);
        channels[clients[clientID].channelID].num_of_clients--;
        clients[clientID].channelID = -1;
    } else {
        send_error("ERR_NOTONCHANNEL", ERR_NOTONCHANNEL, clientID, clients);
    }

    return;
}

void cmd_list(char *prefix, char **params, int n_params, int clientID, client *clients, channel *channels, fd_set *master)
{
    char channel[MAX_MSG_LEN], all_channels[MAX_MSG_LEN];
    memset(channel, '\0', sizeof(channel));
    memset(all_channels, '\0', sizeof(all_channels));

    int total = 0, i;
    for (i = 0; i < MAX_CLIENTS; i++) {
        if (channels[i].num_of_clients > 0) {
            total++;
            sprintf(channel, ":%d: <%s> %d", channels[i].ID, channels[i].name, channels[i].num_of_clients);
            strcat(all_channels, channel);  // Concatenating the string
        }
    }
    if (total == 0) {
        send_message("No active channels.", clientID, clients);
        return;
    }

    send_message(all_channels, clientID, clients);
    return;
}

// Send private message between clients. The first parameter (params[0]) is the receipent client/channel and the second parameter (params[1]) is the message.
void cmd_privmsg(char *prefix, char **params, int n_params, int clientID, client *clients, channel *channels, fd_set *master)
{
    char message[MAX_MSG_LEN];
    memset(message, '\0', sizeof(message));

    int channelID = -1;
    int receiverID = -1;
    printf("params[0]: %s params[1]: %s\n", params[0], params[1]);
    //printf("Client %d:\nNICK %s\nCHANNELID %d\nCHANNEL %s\n", clientID, clients[clientID].nick, clients[clientID].channelID, channels[clients[clientID].channelID].name);
    //printf("Clients on channel: %d\n", channels[clients[clientID].channelID].num_of_clients);
    int i;
    for (i = 0; i < MAX_CLIENTS; i++) {
        // Check if client is sending message to a channel
        if ( (channels[i].num_of_clients > 0) && (strcmp(channels[i].name, params[0]) == 0) ) {
            channelID = channels[i].ID;
            break;
        }
    }

    // Check whether client sends message to the entire channel
    if (channelID > -1) {
        sprintf(message, ":%s PRIVMSG %s :%s", clients[clientID].nick, params[0], params[1]);
        broadcast(message, clients, channelID, channels[channelID].num_of_clients);
    } else {    // If client sends message to another client
        for (i = 0; i < MAX_CLIENTS; i++) {
            // Find the nick of the receiver client
            if (clients[i].registered > 0 && (strcasecmp(clients[i].nick, params[0]) == 0) ) {
                send_privmsg(params[1], clientID, i, clients);
                receiverID = i;
                break;
            }
        }
        
        // If no such nick is found, send an error
        if (receiverID < 0) {
            send_error("ERR_NOSUCHNICK", ERR_NOSUCHNICK, clientID, clients);
        }
    }

    return;
}

// WHO returns all clients connected to the specified channel, i.e., in params[0]
void cmd_who(char *prefix, char **params, int n_params, int clientID, client *clients, channel *channels, fd_set *master)
{
    char user[MAX_MSG_LEN];
    char all_users[MAX_MSG_LEN];
    memset(user, '\0', sizeof(user));
    memset(all_users, '\0', sizeof(all_users));
    
    int channelID;

    int i;
    for (i = 0; i < MAX_CLIENTS; i++) {
        if (strcmp(channels[i].name, params[0]) == 0 && channels[i].num_of_clients > 0) {
            channelID = channels[i].ID;
            break;
        }
    }
    if (i == MAX_CLIENTS) {
        send_error("ERR_NOSUCHCHANNEL", ERR_NOSUCHCHANNEL, clientID, clients);
        return;
    }

    // Find all clients connected to the channel and return their information (NICK, USER and REALNAME)
    if (channelID > -1) {
        for (i = 0; i < MAX_CLIENTS; i++) {
            if (clients[i].channelID == channelID) {
                sprintf(user, "NICK: %s USER: %s REALNAME: %s\n", clients[i].nick, clients[i].user, clients[i].realname);
                strcat(all_users, user);
            }
        }
    }

    send_message(all_users, clientID, clients);
    return;
}