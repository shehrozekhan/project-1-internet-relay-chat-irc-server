#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <error.h>
#include "debug.h"
#include "rtlib.h"
#include "rtgrading.h"
#include "sircd.h"
#include "irc_proto.h"

#define PORT_NUM	6667	// Port number as specified
#define MAXLINE		4096	// Maximum length of text (line)

u_long curr_nodeID;
rt_config_file_t   curr_node_config_file;  /* The config_file  for this node */
rt_config_entry_t *curr_node_config_entry; /* The config_entry for this node */

void init_node(char *nodeID, char *config_file);
void irc_server();

client init_client();
void init_channels(channel *channels);

void
usage() {
    fprintf(stderr, "sircd [-h] [-D debug_lvl] <nodeID> <config file>\n");
    exit(-1);
}


/* Write "n" bytes to a descriptor. */
ssize_t writen(int fd, const void *vptr, size_t n)
 {
     size_t nleft;
     ssize_t nwritten;
     const char *ptr;

     ptr = vptr;
     nleft = n;
     while (nleft > 0) {
         if ( (nwritten = write(fd, ptr, nleft)) <= 0) {
             if (nwritten < 0 && errno == EINTR)
                 nwritten = 0;   /* and call write() again */
             else
                 return (-1);    /* error */
          }

          nleft -= nwritten;
          ptr += nwritten;
     }
     return (n);
}

void err_sys(const char* x){
    perror(x);
    exit(-1);
}

void err_quit(const char* x){
    err_sys(x);
}

int main( int argc, char *argv[] )
{
    extern char *optarg;
    extern int optind;
    int ch;

    while ((ch = getopt(argc, argv, "hD:")) != -1)
        switch (ch) {
	case 'D':
	    if (set_debug(optarg)) {
		exit(0);
	    }
	    break;
        case 'h':
        default: /* FALLTHROUGH */
            usage();
        }
    argc -= optind;
    argv += optind;

    if (argc < 2) {
	usage();
    }
    
    init_node(argv[0], argv[1]);
    
    printf( "I am node %d and I listen on port %d for new users\n", (int) curr_nodeID, (int) curr_node_config_entry->irc_port );

    /* Start your engines here! */

    irc_server();
    
    return 0;
}

void irc_server() {
	int socketFD, clientFD, maxi, maxFD, connFD, sockFD;
	int nready;//, client[FD_SETSIZE];
	ssize_t n;
	fd_set temp, master;					// Master FD list and temporary FD list (for select())
	char buf[MAXLINE];						// Our buffer	
	socklen_t clilen;						// Length of client address
	struct sockaddr_in cliaddr, servaddr;	// Structure that holds client and server IP addresses
	int i;									// Iterator
	
	client clients[MAX_CLIENTS];		// Array that stores client nodes
	channel channels[MAX_CLIENTS];		// Array that stores all channels (one per client)
	
	// Initialize socket
	socketFD = socket(AF_INET, SOCK_STREAM, 0); 
	if (socketFD < 0) {
		err_sys("Error with socket initialization");
	}
	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(PORT_NUM);
	
	// Binds the socket so that it's listening for connections
	if (bind(socketFD, (struct sockaddr *) &servaddr, sizeof(servaddr)) != 0) {
		err_sys("Error in assigning socket address to socket file descriptor");
	}

	if (listen(socketFD, MAX_CLIENTS) != 0) {
		err_sys("Error in accepting connections");
	}
	
	// Initializing
	maxFD = socketFD;
	maxi = -1;
	
	// Initialize our client array. -1 represents an available position
	for (i = 0; i < MAX_CLIENTS; i++) {
		clients[i] = init_client();
	}
	init_channels(channels);
	
	// Clear our master FD list and add our socketFD to it
	FD_ZERO(&master);
	FD_SET(socketFD, &master);

	while (1) {
		// Copy our master FD list to a temp FD list
		temp = master;
		if ((nready = select(maxFD + 1, &temp, NULL, NULL, NULL)) == -1) {
			err_sys("Error in select");
		}
		
		// Check to see if our socketFD is a part of our (temp) FD list
		if (FD_ISSET(socketFD, &temp)) {
			// Handle new connection
			clilen = sizeof(cliaddr);
			if ((connFD = accept(socketFD, (struct sockaddr *) &cliaddr, &clilen)) == -1) {
				err_sys("Error in accept");
			}
			
			// Loop through our client array and save our connection at the first available position
			for (i = 0; i < MAX_CLIENTS; i++) {
				if(clients[i].sock < 0) {
					clients[i].sock = connFD;
					break;
				}
			}
			if (i == MAX_CLIENTS) err_quit("too many clients");
				
			// Add our connection to master FD list
			FD_SET(connFD, &master);
				
			// Keeping track of our maximum FD
			if (connFD > maxFD) maxFD = connFD;
				
			// Keeping track of our maximum index into the client array
			if (i > maxi) maxi = i;

			// Continue until there are no more readable FDs
			if (--nready <= 0) continue;
		}
		
		// Loop through our client array to read data from existing connections
		for (i = 0; i <= maxi; i++) {
			// Check if our client array contains the FD of a connection
			if ((sockFD = clients[i].sock) < 0) continue;

			if (FD_ISSET(sockFD, &temp)) {

				// If the connection is closed, remove it from our (master) FD list and update the data structures
				// accordingly
				bzero(buf, MAXLINE);
				if ( (n = read(sockFD, buf, MAXLINE)) == 0) {
					handle_line("QUIT", i, &clients, channels, &master);
				} 

				// Otherwise, handle_line is called to parse user commands				
				else {
					strtok(buf, "\r\n");
					handle_line(buf, i, &clients, channels, &master);
				}
				//bzero(buf, MAXLINE);
				// Stop if no more readable descriptors
				if (--nready <= 0) break;
			}
		}
	}
	
}

/*
 * void init_node( int argc, char *argv[] )
 *
 * Takes care of initializing a node for an IRC server
 * from the given command line arguments
 */
void
init_node(char *nodeID, char *config_file) 
{
    int i;

    curr_nodeID = atol(nodeID);
    rt_parse_config_file("sircd", &curr_node_config_file, config_file );

    /* Get config file for this node */
    for( i = 0; i < curr_node_config_file.size; ++i )
        if( curr_node_config_file.entries[i].nodeID == curr_nodeID )
             curr_node_config_entry = &curr_node_config_file.entries[i];

    /* Check to see if nodeID is valid */
    if( !curr_node_config_entry )
    {
        printf( "Invalid NodeID\n" );
        exit(1);
    }
}

/* Initialize client node */
client init_client() {
	client node;
	
	node.sock = -1;
	bzero(&node.cliaddr, sizeof(node.cliaddr));
	node.user_registered = -1;	// No user set
	node.nick_registered = -1;	// No nick set
	node.registered = -1;		// Not registered yet
	node.channelID = -1;		// No channel

	// Init empty strings
	memset(node.hostname, '\0', sizeof(node.hostname));
	memset(node.servername, '\0', sizeof(node.servername));
	memset(node.user, '\0', sizeof(node.user));
	memset(node.nick, '\0', sizeof(node.nick));
	memset(node.realname, '\0', sizeof(node.realname));
	memset(node.inbuf, '\0', sizeof(node.inbuf));
	memset(node.channel, '\0', sizeof(node.channel));

	return node;
}

/* Initialize channels array */
void init_channels(channel *channels) {
	int i;
	for (i = 0; i < MAX_CLIENTS; i++) {
		channels[i].ID = i;
		channels[i].num_of_clients = 0;
	}
}